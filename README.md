# Mobilna pasieka


## Skład zespołu
- Mateusz Rasmus
- Michał Laskowski
- Łukasz Błocki
- Julia Szymanek

## Temat projektu
Tematem naszego projektu będzie „Mobilna pasieka”, czyli system harmonogramujący dystrybucję uli na dostępne pola uprawne. System musi uwzględniać nie tylko liczbę posiadanych uli oraz listę pól, ale również roślin na nich uprawianych, terminów i długości kwitnienia, a także wydajność miodową poszczególnych roślin. Na podstawie wspomnianych danych, zadaniem systemu będzie dostarczanie firmie pszczelarskiej najoptymalniejszego rozwiązania jeśli chodzi o rozmieszczenie uli w czasie trwania całego sezonu pszczelarskiego. 

## Cel projektu
Zadaniem systemu będzie zmaksymalizowanie produkcji miodu przy dostępnej liczbie uli oraz rodzajów pożytków dla pszczół (pól uprawnych z wysianymi konkretnymi roślinami).

## Zakres projektu
Projekt będzie obejmował stworzenie podstawowego oraz rozszerzonego modelu matematycznego, a także ich implementacji w przystosowanym do tego środowisku. 
Model podstawowy będzie uwzględniał liczbę uli, dostępne pola uprawne, ich wielkość oraz rośliny na nich uprawiane (co implikuje okres kwitnienia oraz wydajność miodową). 
W modelu rozszerzonym można dodatkowo uwzględnić koszty związane z transportem pszczół oraz z korzystaniem z pól uprawnych.
